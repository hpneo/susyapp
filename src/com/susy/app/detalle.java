package com.susy.app;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.susy.app.R;
import com.susy.app.inicio.DatosLoader;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class detalle extends Activity {
	private String contenido = "";
	private String ruta = "";
	private String carpeta = "/mysonidos/";
	private int inicio = 0;
	public String fileName;
	public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
	private FileOutputStream f;
	private InputStream in;
	private TextView cont;
	private ProgressDialog mProgressDialog;
	public FileDescriptor fileDescriptor;
	ExecutorService executorService;
	private String tboton;
	private int lee = 1;
	private String p1 = "play";
	int band = 0;
	int alt = 1024;
	String id = "";
	private int width; // deprecated
	private int height;
	private int tamano = 0;
	private String url = "";
	public Dialog dialog;
	private  SharedPreferences.Editor editor ;
	File rootDir = Environment.getExternalStorageDirectory();
	private String mensajef = "Llego SusyApp!, la nueva aplicacion oficial de Susy Diaz! Diviertete con todas sus ocurrencias y los mejores concejos gracias al SusyApp. Compartela con tus amigos ya!";
	private String mensajet = "Llego SusyApp!, la nueva aplicacion de Susy Diaz! Diviertete con todas sus ocurrencias gracias al SusyApp. Descargalo aqui";
	private MediaPlayer mediaPlayer = new MediaPlayer();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.detallechiste);

		Display display = getWindowManager().getDefaultDisplay();
		width = display.getWidth(); // deprecated
		height = display.getHeight(); // deprecated
		Bundle bundle = this.getIntent().getExtras();
		id = bundle.getString("iddetalle");
		SharedPreferences settings = getSharedPreferences("susyapp", 0);
		tboton = settings.getString("tboton", "");
		dialog = new Dialog(this);
		editor = settings.edit(); 
		cargado();
		// cargado2();
		executorService = Executors.newFixedThreadPool(1);
		executorService.submit(new DatosLoader());
		cont = (TextView) findViewById(R.id.chiste);
		Typeface font = Typeface.createFromAsset(getAssets(), "segoepr.ttf");
		cont.setTypeface(font);
		cont.setText(contenido);
		int tam = ruta.split("/").length - 1;
		fileName = ruta.split("/")[tam];
		Log.i("dato", fileName);
		new DownloadFileAsync().execute(ruta);
		
//		Toast.makeText(getApplicationContext(), ruta, Toast.LENGTH_LONG).show();

		checkAndCreateDirectory(carpeta);
		final Button play = (Button) findViewById(R.id.play);
		final Button stop = (Button) findViewById(R.id.stop);
		stop.setEnabled(false);
		play.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				executorService.shutdown();
				if (lee == 1) {
					stop.setEnabled(true);
					if (p1.equalsIgnoreCase("play")) {
						p1 = "stop";

						if (height < alt)
							play.setBackgroundResource(R.drawable.btn_pause);
						else
							play.setBackgroundResource(R.drawable.btn_pause2);
						if (inicio == 0) {
							inicio = 1;
							mediaPlayer.reset();
							try {
								FileInputStream fileInputStream = new FileInputStream(
										rootDir + carpeta + fileName);
								fileDescriptor = fileInputStream.getFD();
								mediaPlayer.setDataSource(ruta);
//								mediaPlayer.setDataSource(fileDescriptor);
								mediaPlayer.prepare();
								mediaPlayer.start();
								mediaPlayer
										.setOnCompletionListener(new OnCompletionListener() {
											public void onCompletion(
													MediaPlayer arg0) {
												p1 = "play";
												if (height < alt)
													play.setBackgroundResource(R.drawable.btn_nplay);
												else
													play.setBackgroundResource(R.drawable.btn_nplay2);
												stop.setEnabled(false);
											}
										});

								fileInputStream.close();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							mediaPlayer.start();
						}

					} else {
						p1 = "play";
						if (height < alt)
							play.setBackgroundResource(R.drawable.btn_nplay);
						else
							play.setBackgroundResource(R.drawable.btn_nplay2);
						mediaPlayer.pause();
					}
				} else {
					Toast.makeText(detalle.this,
							"El audio no se encuentra disponible",
							Toast.LENGTH_SHORT).show();
				}

			}
		});

		stop.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				stop.setEnabled(false);
				mediaPlayer.reset();
				FileInputStream fileInputStream;
				try {
					fileInputStream = new FileInputStream(rootDir + carpeta
							+ fileName);
					fileDescriptor = fileInputStream.getFD();
					mediaPlayer.setDataSource(fileDescriptor);
					mediaPlayer.prepare();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				p1 = "play";
				if (height < alt)
					play.setBackgroundResource(R.drawable.btn_nplay);
				else
					play.setBackgroundResource(R.drawable.btn_nplay2);
			}
		});
		ImageView facebook = (ImageView) findViewById(R.id.facebook);
		facebook.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				Intent intent = new Intent();
				intent.setComponent(new ComponentName(detalle.this,
						ShareOnFacebook.class));
				intent.putExtra("facebookMessage", contenido + " " + mensajef);
				intent.putExtra("url", url);
				detalle.this.startActivity(intent);
			}
		});
		ImageView twitter = (ImageView) findViewById(R.id.twitter);
		twitter.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				Intent i = new Intent(Intent.ACTION_VIEW);

				if (contenido.length() > 39) {
					String mensaje = contenido.substring(0, 39) + "... "
							+ mensajet;
					String tweetUrl = "https://twitter.com/intent/tweet?text="
							+ mensaje + " &url=" + "http://bit.ly/P1XD8r";
					Uri uri = Uri.parse(tweetUrl);
					startActivity(new Intent(Intent.ACTION_VIEW, uri));

				} else {
					String mensaje = contenido + " " + mensajet;

					String tweetUrl = "https://twitter.com/intent/tweet?text="
							+ mensaje + " &url=" + "http://bit.ly/P1XD8r";
					Uri uri = Uri.parse(tweetUrl);
					startActivity(new Intent(Intent.ACTION_VIEW, uri));
				}
				startActivity(i);
			}
		});
		ImageView back = (ImageView) findViewById(R.id.volver);
		back.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				if (tboton.equalsIgnoreCase("1")) {
					dialog = ProgressDialog.show(detalle.this, "",
							"Cargando...", true);
					Intent i = new Intent(detalle.this, opcion1.class);
					editor.putString("tboton", "1");
					editor.commit();
					startActivity(i);
				}
				if (tboton.equalsIgnoreCase("2")) {
					dialog = ProgressDialog.show(detalle.this, "",
							"Cargando...", true);
					Intent i = new Intent(detalle.this, opcion1.class);
					editor.putString("tboton", "2");
					editor.commit();
					startActivity(i);
				}
				if (tboton.equalsIgnoreCase("3")) {
					dialog = ProgressDialog.show(detalle.this, "",
							"Cargando...", true);
					Intent i = new Intent(detalle.this, opcion1.class);
					editor.putString("tboton", "3");
					editor.commit();
					startActivity(i);
				}
				if (tboton.equalsIgnoreCase("4")) {
					dialog = ProgressDialog.show(detalle.this, "",
							"Cargando...", true);
					Intent i = new Intent(detalle.this, opcion1.class);
					editor.putString("tboton", "4");
					editor.commit();
					startActivity(i);
				}
			}
		});
	}

	class DownloadFileAsync extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			showDialog(DIALOG_DOWNLOAD_PROGRESS);
		}

		@Override
		protected String doInBackground(String... aurl) {
			try {
				// connecting to url
				URL u = new URL(aurl[0]);
				HttpURLConnection c = (HttpURLConnection) u.openConnection();
				c.setRequestMethod("GET");
				c.setDoOutput(true);
				c.connect();

				// lenghtOfFile is used for calculating download progress
				int lenghtOfFile = c.getContentLength();

				// this is where the file will be seen after the download
				File fl = new File(rootDir + carpeta, fileName);
				if (!fl.exists()) {
					f = new FileOutputStream(fl);

					// file input is from the url
					in = c.getInputStream();
					// here's the download code
					byte[] buffer = new byte[1024];
					int len1 = 0;
					long total = 0;

					while ((len1 = in.read(buffer)) > 0) {
						total += len1; // total = total + len1
						publishProgress(""
								+ (int) ((total * 100) / lenghtOfFile));
						f.write(buffer, 0, len1);
						Log.i("lee", "a");
						lee = 1;
					}
					f.close();
				}
			} catch (Exception e) {
				Log.d("excepcion", e.toString());
			}

			return null;
		}

		protected void onProgressUpdate(String... progress) {
			Log.d("excepcion", progress[0]);
			mProgressDialog.setProgress(Integer.parseInt(progress[0]));
		}

		@Override
		protected void onPostExecute(String unused) {
			// dismiss the dialog after the file was downloaded
			// dialog=new Dialog(Formulario1.this);

			dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
			// dialog.setCancelable(false);
			inicio = 0;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			band = 1;
			try {
				if (mediaPlayer.isPlaying())
					mediaPlayer.reset();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (keyCode == KeyEvent.KEYCODE_HOME) {
			band = 1;
			try {
				if (mediaPlayer.isPlaying())
					mediaPlayer.reset();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onPause() {
		if (dialog.isShowing()) {
			dialog.dismiss();
			Log.i("mensaje", "esta corriendo");
		} else {
			Log.i("mensaje", "no esta corriendo");
		}
		executorService.shutdown();
		if (band == 1) {
			Log.i("mensaje", "mensaje");
			File new_dir = new File(rootDir + carpeta);
			if (new_dir.exists()) {
				String[] children = new_dir.list();
				for (int i = 0; i < children.length; i++) {
					new File(new_dir, children[i]).delete();
				}
			}
		}
		super.onPause();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_DOWNLOAD_PROGRESS: // we set this to 0
			mProgressDialog = new ProgressDialog(this);
			mProgressDialog.setMessage("Cargando Audio...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setMax(100);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
			return mProgressDialog;
		default:
			return null;
		}
	}

	class DatosLoader implements Runnable {
		DatosLoader() {
		}

		public void run() {
			cargado2();
		}
	}

	public void cargado() {
		List<String> miLista = Arrays.asList(new String[] { "C", "R" });
		RssParserSax saxparser = null;
		if (tboton.equalsIgnoreCase("1")) {
			saxparser = new RssParserSax(getResources().getString(R.string.r1)
					+ "?id=" + id, miLista);
		}
		if (tboton.equalsIgnoreCase("2")) {
			saxparser = new RssParserSax(getResources().getString(R.string.r2)
					+ "?id=" + id, miLista);
		}
		if (tboton.equalsIgnoreCase("3")) {
			saxparser = new RssParserSax(getResources().getString(R.string.r3)
					+ "?id=" + id, miLista);
		}
		if (tboton.equalsIgnoreCase("4")) {
			saxparser = new RssParserSax(getResources().getString(R.string.r4)
					+ "?id=" + id, miLista);
		}
		Log.i("dato", tboton);
		List<String> datos = saxparser.parse();
		if (datos.size() > 1) {
			contenido = datos.get(0);
			ruta = datos.get(1);
		} else {
			Log.i("tama�o", String.valueOf(datos.size()));
			Toast.makeText(
					this,
					"Se necesita una conexi�n a internet para poder escuchar los audios",
					Toast.LENGTH_LONG).show();
		}
	}

	public void cargado2() {
		List<String> miLista = Arrays.asList(new String[] { "MC", "LCV" });
		RssParserSax saxparser = null;
		saxparser = new RssParserSax(getResources().getString(R.string.m)
				+ "?app=FB&tv=MC", miLista);
		List<String> datos = saxparser.parse();

		RssParserSax saxparser2 = null;
		saxparser2 = new RssParserSax(getResources().getString(R.string.m)
				+ "?app=TW&tv=MC", miLista);
		List<String> datos2 = saxparser2.parse();

		RssParserSax saxparser3 = null;
		saxparser3 = new RssParserSax(getResources().getString(R.string.url),
				miLista);
		List<String> datos3 = saxparser3.parse();

		if (datos.size() > 0) {
			mensajef = datos.get(0);
		}
		if (datos2.size() > 0) {
			mensajet = datos2.get(0);
		}

		if (datos3.size() > 0) {
			url = datos3.get(0);
		}
	}

	public void checkAndCreateDirectory(String dirName) {
		File new_dir = new File(rootDir + dirName);
		if (!new_dir.exists()) {
			new_dir.mkdirs();
		}
	}
}
