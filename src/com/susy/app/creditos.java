package com.susy.app;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import com.susy.app.R;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class creditos extends Activity{
	private  SharedPreferences.Editor editor ;
	private SharedPreferences settings ;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.creditos);
        settings = getSharedPreferences("susyapp", 0);
        editor = settings.edit();  
        
        ImageView back = (ImageView) findViewById(R.id.volver);
    	back.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View arg0) {
    				Intent i = new Intent(creditos.this, inicio.class);
    				editor.putString("opcion", "O");
                    editor.commit();
    				startActivity(i);
    		}
    	});
	}
	
}
