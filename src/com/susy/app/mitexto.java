package com.susy.app;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

public class mitexto extends TextView {

	Context c;
	Typeface font;
	public mitexto(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		this.setTextColor(Color.TRANSPARENT);
		c=context;
		font=Typeface.createFromAsset(c.getAssets(), "segoepr.ttf");
	}
	public mitexto(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.setTextColor(Color.TRANSPARENT);
		c=context;
		font=Typeface.createFromAsset(c.getAssets(), "segoepr.ttf");
	}
	@Override
	protected void onDraw(Canvas canvas) {
	    super.onDraw(canvas);
	    Log.i("onDraw", "paso");
	    float size = this.getTextSize();
	    String text = (String)this.getText();
	    
	    Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	    textPaint.setColor(Color.parseColor("#FFFFFF"));
	    textPaint.setTypeface(font);
	    textPaint.setTextSize(size);
	    
	    Rect bounds = new Rect();
	    
	    Paint stkPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	    stkPaint.setStyle(Style.STROKE);
	    stkPaint.setStrokeWidth(6);
	    stkPaint.setColor(Color.parseColor("#764C28"));
	    stkPaint.setTypeface(font);
	    stkPaint.setTextSize(size);
	    stkPaint.getTextBounds(text, 0, text.length(), bounds);
	    canvas.drawText(text, (this.getWidth()-bounds.width())/2, (this.getHeight()+bounds.height())/2, stkPaint);

	    
	    textPaint.getTextBounds(text, 0, text.length(), bounds);
	    canvas.drawText(text, (this.getWidth()-bounds.width())/2, (this.getHeight()+bounds.height())/2, textPaint);
	}
}
