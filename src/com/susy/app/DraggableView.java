package com.susy.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;

public class DraggableView {
  private Bitmap img;
  private int x;
  private int y;
  private float scalingFactor;
  
  public DraggableView(Context context, Bitmap img) {
    this.img = img;
  }
  
  public DraggableView(Context context, Bitmap img, Point point) {
    this.img = img;
    this.x = point.x;
    this.y = point.y;
  }
  
  public int getX() {
    return this.x;
  }
  
  public int getY() {
    return this.y;
  }
  
  public Bitmap getImage() {
    return this.img;
  }
  
  public float getScalingFactor() {
    return this.scalingFactor;
  }
  
  public void setX(int x) {
    this.x = x;
  }
  
  public void setY(int y) {
    this.y = y;
  }
  
  public void setScalingFactor(float scalingFactor) {
    this.scalingFactor = scalingFactor;
  }
}
