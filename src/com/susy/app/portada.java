package com.susy.app;

import java.util.Timer;
import java.util.TimerTask;

import com.susy.app.R;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.RelativeLayout;

public class portada extends Activity {
	Timer timer1;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    setContentView(R.layout.portada);    
	}
	
	public void onStart(){
		super.onStart();
		TimerTask desaparecer = new TimerTask(){ //Aqu� declaramos lo que se har� en un hilo cada vez que transcurra la frecuencia
			 public void run() {
				finish();
			 }
		};
		timer1 = new Timer(); //Aqui inicializamos el Timer
		timer1.schedule(desaparecer, 3000);
    }
    public boolean onTouchEvent(MotionEvent event){
        this.finish();
        return true;
    }   
    
}
