package com.susy.app;

import com.susy.app.RestClient.RequestMethod;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ContestActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_contest);

    final EditText txtConcursoEmail = (EditText) findViewById(R.id.txtConcursoEmail);
    final EditText txtConcursoNombres = (EditText) findViewById(R.id.txtConcursoNombres);
    final EditText txtConcursoApellidos = (EditText) findViewById(R.id.txtConcursoApellidos);
    final EditText txtConcursoDNI = (EditText) findViewById(R.id.txtConcursoDNI);
    final EditText txtConcursoDireccion = (EditText) findViewById(R.id.txtConcursoDireccion);
    final EditText txtConcursoTelefono = (EditText) findViewById(R.id.txtConcursoTelefono);

    Button buttonSubmitContest = (Button) findViewById(R.id.buttonSubmitContest);
    buttonSubmitContest.setOnClickListener(new View.OnClickListener() {

      public void onClick(View v) {
        String concursoEmail = txtConcursoEmail.getText().toString().trim();
        String concursoNombres = txtConcursoNombres.getText().toString().trim();
        String concursoApellidos = txtConcursoApellidos.getText().toString()
            .trim();
        String concursoDNI = txtConcursoDNI.getText().toString().trim();
        String concursoDireccion = txtConcursoDireccion.getText().toString()
            .trim();
        String concursoTelefono = txtConcursoTelefono.getText().toString()
            .trim();

        boolean valid = true;

        if (concursoEmail.length() == 0) {
          valid = false;
        }
        if (concursoNombres.length() == 0) {
          valid = false;
        }
        if (concursoApellidos.length() == 0) {
          valid = false;
        }
        if (concursoDNI.length() == 0) {
          valid = false;
        }
        if (concursoDireccion.length() == 0) {
          valid = false;
        }
        if (concursoTelefono.length() == 0) {
          valid = false;
        }

        if (valid) {
          RestClient rest = new RestClient(
              "http://susy-backend.herokuapp.com/participants");
          rest.AddParam("participant[email]", concursoEmail);
          rest.AddParam("participant[first_name]", concursoNombres);
          rest.AddParam("participant[last_name]", concursoApellidos);
          rest.AddParam("participant[dni]", concursoDNI);
          rest.AddParam("participant[address]", concursoDireccion);
          rest.AddParam("participant[telephone]", concursoTelefono);

          try {
            rest.Execute(RequestMethod.POST);

            AlertDialog.Builder dialog = new AlertDialog.Builder(
                ContestActivity.this);
            dialog.setTitle("Registro completado");
            dialog
                .setMessage("¡Gracias! Tus datos han sido agregados al concurso");
            dialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                  }
                });
            AlertDialog alert = dialog.create();
            alert.show();
          } catch (Exception e) {

            AlertDialog.Builder dialog = new AlertDialog.Builder(
                ContestActivity.this);
            dialog.setTitle("Error al completar registro");
            dialog
                .setMessage("Al parecer hubo un error al intentar registrar tus datos. Prueba más tarde");
            dialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                  public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                  }
                });
            AlertDialog alert = dialog.create();
            alert.show();
            e.printStackTrace();
          }
        }
      }
    });
  }

}
