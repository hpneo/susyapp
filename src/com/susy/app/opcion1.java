package com.susy.app;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import com.susy.app.R;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class opcion1 extends Activity {
	private String[] cade1 = new String[0];
	private String[] cade2 = new String[0];
	public Dialog dialog;
	private LinearLayout lyb;
	public String fileURL;
	private int i;
	private String tboton;
	private String c = "";// texto chiste
	private int band = 0;
	private  SharedPreferences.Editor editor ;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		SharedPreferences settings = getSharedPreferences("susyapp", 0);
		settings = getSharedPreferences("susyapp", 0);
        editor = settings.edit();  
		tboton = settings.getString("tboton", "");
		if (tboton.equals("1")) {
			setContentView(R.layout.opcion1_1);
			band = 1;
		}
		if (tboton.equals("2")) {
			setContentView(R.layout.opcion1_2);
			band = 2;
		}
		if (tboton.equals("3")) {
			setContentView(R.layout.opcion1_3);
			band = 3;
		}
		if (tboton.equals("4")) {
			setContentView(R.layout.opcion1_4);
			band = 4;
		}
		ImageView back = (ImageView) findViewById(R.id.regresar);
		back.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				//finish();
				Intent i = new Intent(opcion1.this, inicio.class);
				editor.putString("opcion", "O");
                editor.commit();
				startActivity(i);
			}
		});
		dialog = new Dialog(this);
		cargado();
		ScrollView sc = (ScrollView) findViewById(R.id.scrollView1);
		LinearLayout layout = (LinearLayout) findViewById(R.id.contenedor);
		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		int height = display.getHeight(); // deprecated
		if (height < 480)
			sc.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, height - 170));
		else {
			if (height < 720)
				sc.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, height - 250));
			else {
				if (height < 1024)
					sc.setLayoutParams(new LinearLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT, height - 430));
			}
		}

		// Toast.makeText(this,String.valueOf(height) ,
		// Toast.LENGTH_LONG).show();
		for (i = 0; i < cade2.length; i++) {

			// lyb.setBackgroundResource(R.drawable.boton);
			// final mitexto mt=new mitexto(this);
			final TextView mt = new TextView(this);
			mt.setOnClickListener(new View.OnClickListener() {
				public void onClick(View arg0) {
					c = (String) mt.getText();
					int j;
					for (j = 0; j < cade2.length; j++) {
						if (cade2[j].equalsIgnoreCase(c)) {
							Log.i("msj", "1");
							break;
						} else {
							Log.i("msj", "1");
						}
					}
					dialog = ProgressDialog.show(opcion1.this, "",
							"Cargando...", true);
					Intent i = new Intent(opcion1.this, detalle.class);
					i.putExtra("iddetalle", cade1[j]);
					startActivity(i);
				}
			});
			Typeface font = Typeface.createFromAsset(getAssets(), "segoepr.ttf");
			mt.setTypeface(font);
			mt.setText(cade2[i]);
			if (band == 4)
				mt.setTextColor(Color.BLACK);
			else
				mt.setTextColor(Color.WHITE);
			mt.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
			// mt.setBackgroundResource(R.drawable.btn_frases);
			if (band == 1)
				mt.setBackgroundResource(R.drawable.btn_frases_p);
			if (band == 2)
				mt.setBackgroundResource(R.drawable.btn_dietas_p);
			if (band == 3)
				mt.setBackgroundResource(R.drawable.btn_exitos_p);
			if (band == 4)
				mt.setBackgroundResource(R.drawable.btn_atrapa_p);

			layout.addView(mt, new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		}
	}

	@Override
	public void onPause() {
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
		super.onPause();
	}

	public void cargado() {
		List<String> miLista;
		RssParserSax saxparser = null;
		if (tboton.equalsIgnoreCase("1")) {
			miLista = Arrays.asList(new String[] { "ID", "T" });
			saxparser = new RssParserSax(getResources().getString(R.string.r1),
					miLista);
		}
		if (tboton.equalsIgnoreCase("2")) {
			miLista = Arrays.asList(new String[] { "ID", "T" });
			saxparser = new RssParserSax(getResources().getString(R.string.r2),
					miLista);
		}
		if (tboton.equalsIgnoreCase("3")) {
			miLista = Arrays.asList(new String[] { "ID", "T" });
			saxparser = new RssParserSax(getResources().getString(R.string.r3),
					miLista);
		}
		if (tboton.equalsIgnoreCase("4")) {
			miLista = Arrays.asList(new String[] { "ID", "T" });
			saxparser = new RssParserSax(getResources().getString(R.string.r4),
					miLista);
		}
		List<String> datos = saxparser.parse();
		if (datos.size() > 1) {
			cade1 = new String[datos.size() / 2];
			cade2 = new String[datos.size() / 2];
			int cont = 0;
			int c1 = 0;
			int c2 = 0;
			for (int h = 0; h < datos.size(); h++) {
				cont++;
				if (cont == 1) {
					cade1[c1] = datos.get(h);
					c1++;
				}
				if (cont == 2) {
					cade2[c2] = datos.get(h);
					c2++;
					cont = 0;
				}
			}
		} else {

			Log.i("tama�o", String.valueOf(datos.size()));
			Toast.makeText(
					this,
					"Se necesita una conexi�n a internet para listar los contenidos",
					Toast.LENGTH_LONG).show();
		}
	}
}
