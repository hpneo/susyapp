package com.susy.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

public class FotoActivity extends Activity {
  private String photo_path;
  private String photo_source;
  private Drawable backDrawable;
  private DragContainer container;

  private Bitmap back;
  private Bitmap sticker;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);

    photo_path = getIntent().getExtras().getString("photo_path");
    photo_source = getIntent().getExtras().getString("photo_source");

    if (!photo_path.equals(null)) {
      Point position = new Point(0, 0);
      BitmapFactory.Options options = new BitmapFactory.Options();

      back = BitmapFactory.decodeFile(photo_path, options);

      if (photo_source.equals("camera") && back.getWidth() > back.getHeight()) {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        back = Bitmap.createBitmap(back, 0, 0, back.getWidth(),
            back.getHeight(), matrix, true);
      }

      sticker = BitmapFactory.decodeResource(getResources(),
          R.drawable.susy_labios);
      // sticker = BitmapFactory.decodeResource(getResources(),
      // R.drawable.sticker_1);

      backDrawable = new BitmapDrawable(getResources(), back);

      container = new DragContainer(this, backDrawable);
      container.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
          LayoutParams.FILL_PARENT));

      position
          .set(
              getResources().getDisplayMetrics().widthPixels
                  - sticker.getWidth(),
              getResources().getDisplayMetrics().heightPixels
                  - sticker.getHeight());
      container.setDraggableView(new DraggableView(this, sticker, position));

      setContentView(container);
    } else {
      setContentView(R.layout.activity_foto);
    }
  }

  @Override
  protected void onDestroy() {
    back.recycle();
    sticker.recycle();
    super.onDestroy();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.foto, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
    case R.id.foto_save:
      // Bitmap background = ((BitmapDrawable)
      // container.getBackground()).getBitmap();
      // Bitmap overlay = container.getDraggableView().getImage();

      // Bitmap result = Bitmap.createBitmap(background.getWidth(),
      // background.getHeight(), background.getConfig());

      container.setDrawingCacheEnabled(true);
      container.destroyDrawingCache();
      Bitmap result = container.getDrawingCache(false);

      if (result.getWidth() > result.getHeight()) {
        Matrix matrix = new Matrix();
        matrix.postRotate(-90);

        result = Bitmap.createBitmap(result, 0, 0, result.getWidth(),
            result.getHeight(), matrix, true);
      }
      // Canvas canvas = new Canvas(result);
      // canvas.drawBitmap(background, 0, 0, null);
      // canvas.drawBitmap(overlay, container.getDraggableView().getX(),
      // container.getDraggableView().getY(), null);

      photo_path = Environment.getExternalStorageDirectory().getPath()
          + "/susyapp-with-sticker-" + new Date().getTime() + ".jpg";
      File file = new File(photo_path);

      try {
        FileOutputStream output = new FileOutputStream(file);
        result.compress(Bitmap.CompressFormat.PNG, 85, output);
        output.flush();
        output.close();

        Intent mediaScanIntent = new Intent(
            Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(new File(photo_path));
        mediaScanIntent.setData(contentUri);

        this.sendBroadcast(mediaScanIntent);
        
        Toast.makeText(getBaseContext(), "Tu imagen fue guardada correctamente", Toast.LENGTH_LONG).show();
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

      return true;
    default:
      return super.onOptionsItemSelected(item);
    }
  }

}
