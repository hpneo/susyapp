package com.susy.app;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import java.net.URL;
import javax.xml.parsers.SAXParser;
import java.net.MalformedURLException;
import javax.xml.parsers.SAXParserFactory;
 
public class RssParserSax
{
    private URL rssUrl;
    final List<String> datos=new ArrayList<String>();
    private List<String> etiquetas=new ArrayList<String>();
    private int sum=0;
    private String tempc="";
    private int tempn=0;
    private String etikextra="";
    private String contextra="";
    public RssParserSax(String url,List<String> etq)
    {
        try
        {
            this.rssUrl = new URL(url);
            etiquetas=etq;
            
        }
        catch (MalformedURLException e)
        {
            System.out.println(e.getMessage());
        }
    }
   
    	
    public List<String> parse()
    {
       
        try
        {
        	 SAXParserFactory factory = SAXParserFactory.newInstance();
             SAXParser parser = factory.newSAXParser();
            
             //RssHandler handler = new RssHandler();
             
             DefaultHandler handler2 = new DefaultHandler() {
                 boolean name = false;
                 boolean namex = false;
                 public void startElement(String uri, String localName, String qName, Attributes attributes)
                     throws SAXException {
                	 Log.i("etq", qName);
                	 Log.i("etiqeta", etiquetas.get(sum).toString());
                	 if(qName.equalsIgnoreCase(tempc))sum=tempn;
                	 if(etikextra.equalsIgnoreCase(qName)){
                		 namex=true;
                	 }
                	 if (qName.equalsIgnoreCase(etiquetas.get(sum).toString())) {
                         name = true;
                         tempc=qName;
                         tempn=sum;
                         if(sum<etiquetas.size()-1)sum++; else sum=0;                         
                     }
                   
                 }

                 public void characters(char ch[], int start, int length) throws SAXException {
                   if (name) {
                	   Log.i("dato", new String(ch, start, length));
                	   datos.add(new String(ch, start, length));                     
                      name = false;
                   }
                   if (namex) {
                	   contextra=new String(ch, start, length);                     
                      namex = false;
                   }
                 }
               };
             
             parser.parse(rssUrl.openConnection().getInputStream(), handler2);
             
              // parser.parse(  new InputSource(new StringReader(xmlString)), handler2);
             return datos;
             //return datos;
        	
        }
        catch (Exception e)
        {
            datos.add(e.getMessage());
            return datos;
        	// throw new RuntimeException(e);
        }
    }
    public String getExtra(){
    	return contextra;
    }
    

}