package com.susy.app;

import java.io.File;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class inicio extends Activity {
  /** Called when the activity is first created. */
  private static final int IMAGE_PICK     = 1;
  private static final int IMAGE_CAPTURE  = 2;
  private static final int CAMERA_REQUEST = 1888;
  int dato = 0;
  public Dialog dialog;
  private SharedPreferences settings;
  private SharedPreferences.Editor editor;
  ExecutorService executorService;
  private String mensajef = "Llego SusyApp!, la nueva aplicacion oficial de Susy Diaz! Diviertete con todas sus ocurrencias y los mejores concejos gracias al SusyApp. Compartela con tus amigos ya!";
  private String mensajet = "Llego SusyApp!, la nueva aplicacion de Susy Diaz! Diviertete con todas sus ocurrencias gracias al SusyApp. Descargalo aqui";
  private String url = "";
  private String photo_path = "";

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.main);
    settings = getSharedPreferences("susyapp", 0);
    editor = settings.edit();
    dialog = new Dialog(this);
    // cargado();
    executorService = Executors.newFixedThreadPool(5);
    executorService.submit(new DatosLoader());
    ImageView info = (ImageView) findViewById(R.id.info);
    info.setOnClickListener(new View.OnClickListener() {

      public void onClick(View v) {
        Intent i = new Intent(inicio.this, creditos.class);
        startActivity(i);
      }
    });
    
    LinearLayout concursa = (LinearLayout) findViewById(R.id.btnConcurso);
    concursa.setOnClickListener(new View.OnClickListener() {
      
      public void onClick(View v) {
        Intent i = new Intent(inicio.this, ContestActivity.class);
        startActivity(i);
      }
    });
    
    LinearLayout camera = (LinearLayout) findViewById(R.id.btnFoto);
    camera.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(inicio.this);
        dialog.setTitle("Elija una opción");
        dialog.setItems(new String[] {
            "Desde galería",
            "Desde cámara"
        }, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int position) {
              Intent intent;
                switch(position) {
                case 0:
                  intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                  intent.setType("image/*");
                  startActivityForResult(Intent.createChooser(intent, "Escoja una foto"), IMAGE_PICK);
                  break;
                case 1:
                  photo_path = Environment.getExternalStorageDirectory().getPath() + "/susyapp-" + new Date().getTime() + ".jpg";
                  intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                  intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(photo_path)));
                  startActivityForResult(intent, IMAGE_CAPTURE);
                  break;
                }
            }

        });
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = dialog.create();
        alert.show();
      }
    });
    
    LinearLayout btn1 = (LinearLayout) findViewById(R.id.btn1);
    btn1.setOnClickListener(new View.OnClickListener() {

      public void onClick(View v) {
        // TODO Auto-generated method stub

        dialog = ProgressDialog.show(inicio.this, "", "Cargando...", true);
        Intent i = new Intent(inicio.this, opcion1.class);
        editor.putString("tboton", "1");
        editor.commit();
        startActivity(i);
      }
    });
    LinearLayout btn2 = (LinearLayout) findViewById(R.id.btn2);
    btn2.setOnClickListener(new View.OnClickListener() {

      public void onClick(View v) {
        // TODO Auto-generated method stub

        dialog = ProgressDialog.show(inicio.this, "", "Cargando...", true);
        Intent i = new Intent(inicio.this, opcion1.class);
        editor.putString("tboton", "2");
        editor.commit();
        startActivity(i);
      }
    });
    LinearLayout btn3 = (LinearLayout) findViewById(R.id.btn3);
    btn3.setOnClickListener(new View.OnClickListener() {

      public void onClick(View v) {
        // TODO Auto-generated method stub

        dialog = ProgressDialog.show(inicio.this, "", "Cargando...", true);
        Intent i = new Intent(inicio.this, opcion1.class);
        editor.putString("tboton", "3");
        editor.commit();
        startActivity(i);
      }
    });
    LinearLayout btn4 = (LinearLayout) findViewById(R.id.btn4);
    btn4.setOnClickListener(new View.OnClickListener() {

      public void onClick(View v) {
        // TODO Auto-generated method stub

        dialog = ProgressDialog.show(inicio.this, "", "Cargando...", true);
        Intent i = new Intent(inicio.this, opcion1.class);
        editor.putString("tboton", "4");
        editor.commit();
        startActivity(i);
      }
    });
    ImageView facebook = (ImageView) findViewById(R.id.facebook);
    facebook.setOnClickListener(new View.OnClickListener() {
      public void onClick(View arg0) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(inicio.this,
            ShareOnFacebook2.class));
        intent.putExtra("facebookMessage", mensajef);
        intent.putExtra("url", url);
        inicio.this.startActivity(intent);
      }
    });

    ImageView twitter = (ImageView) findViewById(R.id.twitter);
    twitter.setOnClickListener(new View.OnClickListener() {
      public void onClick(View arg0) {
        String tweetUrl = "https://twitter.com/intent/tweet?text=" + mensajet
            + " &url=" + "http://bit.ly/P1XD8r";
        Uri uri = Uri.parse(tweetUrl);
        startActivity(new Intent(Intent.ACTION_VIEW, uri));
      }
    });
    SharedPreferences settings = getSharedPreferences("susyapp", 0);
    String opcion = settings.getString("opcion", "");
    if (!opcion.equalsIgnoreCase("O")) {
      Intent i = new Intent(this, portada.class);
      startActivity(i);
    }

  }

  class DatosLoader implements Runnable {
    DatosLoader() {
    }

    public void run() {
      cargado();
    }
  }

  public void cargado() {
    List<String> miLista = Arrays.asList(new String[] { "MA" });
    List<String> miLista2 = Arrays.asList(new String[] { "url" });
    RssParserSax saxparser = null;
    saxparser = new RssParserSax(getResources().getString(R.string.m)
        + "?app=FB&tv=MA", miLista);
    List<String> datos = saxparser.parse();

    RssParserSax saxparser2 = null;
    saxparser2 = new RssParserSax(getResources().getString(R.string.m)
        + "?app=TW&tv=MA", miLista);
    List<String> datos2 = saxparser2.parse();

    RssParserSax saxparser3 = null;
    saxparser3 = new RssParserSax(getResources().getString(R.string.url),
        miLista2);
    List<String> datos3 = saxparser3.parse();

    if (datos.size() > 0) {
      mensajef = datos.get(0);
    }
    if (datos2.size() > 0) {
      mensajet = datos2.get(0);
    }
    if (datos3.size() > 0) {
      url = datos3.get(0);
    }
    Log.i("url", url);
  }
  
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == Activity.RESULT_OK) {
      Intent i = new Intent(inicio.this, FotoActivity.class);
      switch (requestCode) {
      case IMAGE_PICK:
        Uri selectedImage = data.getData();
        String [] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        photo_path = cursor.getString(columnIndex);
        cursor.close();
        i.putExtra("photo_source", "gallery");
        break;
      case IMAGE_CAPTURE:
        i.putExtra("photo_source", "camera");
        break;
      }
      
      i.putExtra("photo_path", photo_path);
      startActivity(i);
//      Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//      Uri contentUri = Uri.fromFile(new File(photo_path));
//      mediaScanIntent.setData(contentUri);
//      this.sendBroadcast(mediaScanIntent);
//      
//      Intent i = new Intent(inicio.this, FotoActivity.class);
//      i.putExtra("photo_path", photo_path);
//      startActivity(i);
    }
  }

  @Override
  public void onPause() {
    if (dialog.isShowing()) {
      dialog.dismiss();
      Log.i("mensaje", "esta corriendo");
    } else {
      Log.i("mensaje", "no esta corriendo");
    }
    super.onPause();
  }

  public void onStart() {
    super.onStart();

  }

}