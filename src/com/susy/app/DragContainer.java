package com.susy.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class DragContainer extends View {
  private Drawable background;
  private DraggableView draggable;
  private Canvas canvas;
  
  public DragContainer(Context context) {
    super(context);
    setFocusable(true);
  }
  
  public DragContainer(Context context, Drawable background) {
    super(context);
    this.background = background;
    this.setBackgroundDrawable(background);
    setFocusable(true);
  }
  
  public void setDraggableView(DraggableView draggable) {
    this.draggable = draggable;
  }
  
  public DraggableView getDraggableView() {
    return this.draggable;
  }
  
  public Drawable getBackground() {
    return this.background;
  }
  
  public Canvas getCanvas() {
    return this.canvas;
  }
  
  @SuppressLint("DrawAllocation")
  @Override
  protected void onDraw(Canvas canvas) {
    this.canvas = canvas;
    canvas.drawBitmap(this.draggable.getImage(), this.draggable.getX(), this.draggable.getY(), null);
    
    super.onDraw(canvas);
  }
  
  @Override
  public boolean onTouchEvent(MotionEvent event) {
    int eventAction = event.getAction();
    
    int X = (int)event.getX(); 
    int Y = (int)event.getY();
    
    switch (eventAction) {
      case MotionEvent.ACTION_MOVE:
        this.draggable.setX(X - (this.draggable.getImage().getWidth() / 2));
        this.draggable.setY(Y - (this.draggable.getImage().getHeight() / 2));
      break;
    }
    
    invalidate(); 
    return true;
  }

}
